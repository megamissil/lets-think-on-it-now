<?php
/*
Template Name: Home
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div id="page-home" role="main">
  <article class="home-main-content" id="hero-anchor">
    <section id="" class="section-housecalls section">
      <?php
         $post   = get_post( 12 );
         $title = $post->post_title;
         $publicTalks = types_render_field( "housecalls-public-talks", array() );
         $professionalCe = types_render_field( "housecalls-professional-ce", array() );
         $housecallsImage = types_render_field( "housecalls-image", array("alt" => "home image", "separator"=>"</div><div>") );
      ?>

      <div class="row">
        <div class="medium-8 medium-centered columns">
          <h1><?php echo $title; ?></h1>
        </div>
      </div>
      <div class="row housecalls-content" data-equalizer data-equalize-on="medium">
        <div class="medium-6 columns housecalls-content-col" data-equalizer-watch>
          <h4><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-hcls-1.png" alt="public talks" />talks for the public</h4>
          <?php echo $publicTalks; ?>
        </div>
        <div class="medium-6 columns" data-equalizer-watch>
          <h4><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-hcls-2.png" alt="public talks" />professional CE</h4>
          <?php echo $professionalCe; ?>
        </div>
      </div>
      <div class="row">
        <div class="large-8 columns carousel-container">
          <div class="housecalls-carousel">
             <div>
                <?php echo types_render_field( 'housecalls-image', array( 'alt' => 'home image', 'separator'=>'</div><div>', 'size' => 'hcls-thumbnail') ) ?>
             </div>
          </div>
        </div>
        <div class="large-4 columns housecalls-email-signup">
          <p>notify me of future talks</p>
          <a href="/email-subscribe/" class="blue-button button">Sign up for emails</a>
        </div>
      </div>

    </section>

    <section id="event-anchor" class="section-speakers section">
      <?php
         $post  = get_post( 14 );
         $title = $post->post_title;
      ?>

      <div class="row">
        <div class="medium-8 medium-centered columns">
          <h1><?php echo $title; ?></h1>
          <h4>your event - our speakers</h4>
        </div>
      </div>
      <div class="white-divide"></div>
      <div class="row">
         <?php
          $types = array ( 'post_type' => 'event-type', 'posts_per_page' => 6 );
          query_posts($types);

          while (have_posts()) : the_post();
         ?>
            <div class="small-6 medium-4 large-2 columns">
               <div class="event-type-col">
                     <span><?php echo types_render_field( "event-type-icon", array() ); ?></span>
                     <h6><?php echo types_render_field( "event-type-name", array() ); ?></h6>
                     <?php echo types_render_field( "event-type-description", array() ); ?>
               </div>
            </div>
         <?php endwhile; ?>
      </div>
      <div class="white-divide"></div>
      <div class="row">
        <div class="medium-6 medium-centered columns text-center">
          <?php
             $post   = get_post( 22 );
             $title = $post->post_title;
             $content = $post->post_content;
          ?>
          <a href="/contact/" class="event-button button">Contact us to speak at your event!</a>

        </div>
      </div>
    </section>


  <!-- Podcasts -->
    <section id="podcasts-anchor" class="section-podcasts section">
      <?php
         $post   = get_post( 16 );
         $title = $post->post_title;
         $content = $post->post_content;
      ?>

      <div class="row">
        <div class="medium-8 medium-centered columns">
          <h1><?php echo $title; ?></h1>
        </div>
      </div>

      <div class="row">
        <div class="large-6 columns">
          <h5>What to expect</h5>
          <?php echo $content; ?>
        </div>
        <div class="cyan-divide hide-for-large"></div>
        <div class="large-6 columns meet-the-hosts-col">
          <h5>Meet the hosts</h5>
          <div class="host-carousel">
            <?php $hosts = array ( 'post_type' => 'podcast-host', 'posts_per_page' => -1 );
            query_posts($hosts);

            while ( have_posts() ) : the_post(); ?>
              <a data-open="post-<?php the_ID(); ?>">
                <div class="host-slide">
                  <div class="blue-overlay"></div>
                  <?php echo types_render_field( "podcast-host-image", array( "alt" => "host image", 'separator'=>'</div><div>') ) ?>
                  <h6><?php echo types_render_field( "podcast-host-name", array( 'separator'=>'</div><div>') ) ?></h6>
                </div>
              </a>

                <div class="reveal" data-reveal id="post-<?php the_ID(); ?>">
                  <h3><?php echo types_render_field( "podcast-host-name", array( 'separator'=>'</div><div>') ) ?></h3>
                  <p><?php echo types_render_field( "podcast-host-image", array( "alt" => "host image", 'separator'=>'</div><div>') ) ?><?php echo types_render_field( "podcast-host-bio", array( 'separator'=>'</div><div>') ) ?></p>
                  <button class="close-button" data-close aria-label="Close modal" type="button">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            <?php endwhile; wp_reset_query(); ?>

          </div>
        </div>
      </div>
      <div class="row">
        <div class="medium-10 medium-centered columns player text-center">
          <hr>
          <?php $podcast = array ( 'post_type' => 'post', 'posts_per_page' => 1 );
            query_posts($podcast);
            while ( have_posts() ) : the_post();
              get_template_part( 'template-parts/content', get_post_format() );
            endwhile; wp_reset_query(); ?>
        </div>
      </div>
      <div class="row">
        <div class="medium-8 large-6 medium-centered columns">
          <div class="row">
            <div class="medium-6 columns">
              <a class="button" href="/podcast-episodes/">more episodes</a>
            </div>
            <div class="medium-6 columns">
              <a class="button" href="/subscribe/" target="_blank">subscribe to feed</a>
            </div>
          </div>
      </div>
    </section>

    <!-- Radio -->
    <section id="radio-anchor" class="section-radio section">
      <?php
         $post   = get_post( 18 );
         $title = $post->post_title;
         $content = $post->post_content;
      ?>

      <div class="row">
        <div class="medium-8 medium-centered columns">
          <h1><?php echo $title; ?></h1>
        </div>
      </div>
      <div class="cyan-divide"></div>
      <div class="row">
        <div class="medium-7 columns radio-content">
          <?php echo $content; ?>
        </div>
        <div class="medium-5 columns">
          <div class="row radio-buttons">
            <div class="large-5 columns mic show-for-large">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/podcast-mic-web.png" alt="logo" />
            </div>
            <div class="large-7 columns bmr">
              <a class="button white-button" href="<?php echo types_render_field( "button-link", array('output' => 'raw') ) ?>" target="_blank"><?php echo types_render_field( "button-text" ) ?></a>
            </div>
          </div>
        </div>
      </div>
      <div class="cyan-divide"></div>
      <div class="row">
        <div class="medium-6 medium-centered columns text-center">
          <a class="button white-button" href="/email-subscribe/">Sign up for emails</a>
        </div>
      </div>
    </section>
  </article>
</div>

<?php get_footer();
