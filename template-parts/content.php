<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry'); ?>>
	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	<div class="pod-content">
		<?php the_content(); ?>
	</div>
	<hr />
</div>