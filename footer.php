<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>
		<div id="footer-container">
			<footer id="footer">
				<div class="row">
					<div class="medium-6 columns copy">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-nav' ) ); ?>
						<p>Copyright &copy; <?=date('Y'); ?> Let's Think On It. All Rights Reserved.</p>
					</div>
					<div class="medium-6 columns">
						<span class="moxy"><a href="http://digmoxy.com" target="_blank">Moxy</a></span>
					</div>
				</div>
				<div class="row">
					<div class="medium-10 columns end">
						<p><small><?php dynamic_sidebar( 'footer-widget' ); ?></small></p>
					</div>
				</div>
			</footer>
		</div>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick/slick.min.js"></script>
<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
