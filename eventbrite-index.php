<?php
/**
 * Template Name: Eventbrite Events
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main subpage" role="main">
			<div class="row">
			    <div class="small-12 columns">
			        <header class="page-header">
			          <h1 class="entry-title"><?php the_title(); ?></h1>
			        </header>
			    </div>
			</div>
			<div class="row">
        		<div class="medium-10 medium-centered columns">
					<?php
						// Set up and call our Eventbrite query.
						$events = new Eventbrite_Query( apply_filters( 'eventbrite_query_args', array(
							// 'display_private' => false, // boolean
							// 'status' => 'live',         // string (only available for display_private true)
							// 'nopaging' => false,        // boolean
							// 'limit' => null,            // integer
							// 'organizer_id' => null,     // integer
							// 'p' => null,                // integer
							// 'post__not_in' => null,     // array of integers
							// 'venue_id' => null,         // integer
							// 'category_id' => null,      // integer
							// 'subcategory_id' => null,   // integer
							// 'format_id' => null,        // integer
						) ) );

						if ( $events->have_posts() ) :
							while ( $events->have_posts() ) : $events->the_post(); ?>

								<article id="event-<?php the_ID(); ?>" <?php post_class(); ?>>
									<header class="entry-header">
										<?php the_post_thumbnail(); ?>

										<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

										<div class="entry-meta">
											<?php eventbrite_event_meta(); ?>
										</div><!-- .entry-meta -->
									</header><!-- .entry-header -->

									<div class="entry-content">
										<?php eventbrite_ticket_form_widget(); ?>
									</div><!-- .entry-content -->

								</article><!-- #post-## -->

							<?php endwhile;

							// Previous/next post navigation.
							eventbrite_paging_nav( $events );

						else :
							// If no content, include the "No posts found" template.
							get_template_part( 'content', 'none' );

						endif;

						// Return $post to its rightful owner.
						wp_reset_postdata();
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
