<?php
/**
 * The sidebar containing the main widget area
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<?php if (is_page('events')) {

} else { ?>
	
<aside class="sidebar">
	<?php if (is_category()) { ?>
		<a href="/podcast-episodes/" class="all-eps">< All Episodes</a>
	<?php } ?>
	
	<?php dynamic_sidebar( 'sidebar-widgets' ); ?>

	<?php do_action( 'foundationpress_after_sidebar' ); ?>
</aside>

<?php } ?>