<?php

/**
 * Safely overrides the function which generates the Blubrry iFrame.
 *
 * PowerPress currently stores podcast data within the "enclosure" post field.
 * Currently, we look to the fourth index for things like the image, duration, etc.
 *
 * @param string $content The <iframe> tag we want to manipulate.
 * @return string
 */
function override_powerpressplayer_player_audio($content) {
    global $post;

    // Retrieve metadata for the post to look for an image
    $meta = get_post_meta($post->ID);

    if ( isset($meta['enclosure']) ) {
        $meta = explode("\n", $meta['enclosure'][0]);
    }

    $meta = unserialize($meta[3]);

    $image = null;

    // Fall back to what was defined in the PowerPress settings if the post doesn't specify one
    if ( !isset($meta['image']) ) {
        $settings = get_option('powerpress_general');

        if ( isset($settings['tag_coverart']) && !empty($settings['tag_coverart']) ) {
            $image = $settings['tag_coverart'];
        }
    } else {
        $image = $meta['image'];
    }

    // Begin writing the query string
    $query = '&podcast_title=' . urlencode($post->post_title);

    if ( !is_null($image) ) {
        $query .= '&artwork_url=' . urlencode($image);
    }

    // Get the value of iframe[src]
    $src = preg_match('/src="([^"]*)"/i', $content, $matches);

    // Replace that value if the expression matched it
    if ( $src !== false ) {
        $url = $matches[1] . $query;

        $content = str_replace($matches[1], $url, $content);
    }

    return $content;
}

add_filter('powerpress_player', 'override_powerpressplayer_player_audio', 20, 1);
