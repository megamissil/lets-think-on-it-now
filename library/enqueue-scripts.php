<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'main-stylesheet', get_template_directory_uri() . '/assets/stylesheets/foundation.css', array(), '2.6.1', 'all' );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );

	// If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
	// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.6.1', true );

	// Add the comment-reply library on pages where it is necessary
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;

//hide Toolset fields from all pages
add_action('admin_enqueue_scripts', 'toolset_admin_styles');
function toolset_admin_styles() {

    if(get_post_type() == 'page' || get_post_type() == 'post') {
        echo '<style type="text/css">
          #types-information-table {display:none;}
        </style>';
    }
}

//hide Housecalls Lecture Series fields from all pages except 'Housecalls Lecture Series'
add_action('admin_enqueue_scripts', 'housecalls_admin_styles');
function housecalls_admin_styles() {
    $arr = array(12);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #wpcf-group-housecalls-lecture-series {display:none;}
        </style>';
    }
    if (in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #postdivrich {display:none;}
        </style>';
    }
}

//hide Event Speaker fields from all pages except 'Event Speakers'
add_action('admin_enqueue_scripts', 'event_admin_styles');
function event_admin_styles() {
    $arr = array(14);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-event-type {display:none;}
        </style>';
    }
    if (in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #postdivrich {display:none;}
        </style>';
    }
}

//hide Podcast Host fields from all pages except 'Podcasts'
add_action('admin_enqueue_scripts', 'host_admin_styles');
function host_admin_styles() {
    $arr = array(16);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #types-child-table-page-podcast-host {display:none;}
        </style>';
    }
}

//hide Radio fields from all pages except 'Radio'
add_action('admin_enqueue_scripts', 'radio_admin_styles');
function radio_admin_styles() {
    $arr = array(18);
    if(get_post_type() == 'page' && !in_array(get_the_ID(), $arr)) {
        echo '<style type="text/css">
          #wpcf-group-radio {display:none;}
        </style>';
    }
}
