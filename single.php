<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="single-post" class="subpage" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
			<div class="row">
			    <div class="small-12 columns">
			        <header>
			          <h1 class="entry-title"><?php the_title(); ?></h1>
			        </header>
			    </div>
			</div>
			<div class="entry-content">
				<div class="row">
					<div class="medium-8 columns">
						<?php
							if ( has_post_thumbnail() ) :
								the_post_thumbnail();
							endif;
						?>

						<?php the_content(); ?>
					</div>
					<div class="medium-4 columns">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</article>

<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>
</div>
<?php get_footer();