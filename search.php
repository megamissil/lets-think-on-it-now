<?php
/**
 * The template for displaying search results pages.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="search-page">
	<div class="row">
	    <div class="medium-12 columns">

			<?php do_action( 'foundationpress_before_content' ); ?>

			<h3><?php _e( 'Search Results for', 'foundationpress' ); ?> "<?php echo get_search_query(); ?>"</h3>

		</div>
	</div>
	<div class="row">
	    <div class="medium-8 columns">
			<?php if ( have_posts() ) : ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
				<?php endwhile; ?>

				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>

			<?php endif;?>

			<?php do_action( 'foundationpress_before_pagination' ); ?>

			<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>

				<nav id="post-nav">
					<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
					<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
				</nav>
			<?php } ?>

			<?php do_action( 'foundationpress_after_content' ); ?>
		</div>
		<div class="medium-4 columns">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>
<?php get_footer();
