<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Fjalla+One|Rubik:400,400i,700,900&subset=latin-ext" rel="stylesheet">

		<!-- slick -->
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick/slick-theme.css"> -->
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/components/slick/slick/slick.css">

		<?php wp_head(); ?>
	</head>
	
	<body <?php body_class(); ?>>
	<?php include_once("google-analytics.php") ?>

	<?php do_action( 'foundationpress_after_body' ); ?>
	<div class="mobile-top-bar">
		<div class="row">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="mobile-nav-logo">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ltoi-logo.png" alt="logo" />
			</a>

			<div class="mobile-nav-icon">
				<a href="javascript:;"><i class="fa fa-bars"></i></a>
			</div>

			<div class="mobile-nav">
				<i class="fa fa-times close"></i>
				<div class="mobile-nav-links">
					<?php wp_nav_menu( array('menu' => 'Mobile' )); ?>
				</div>
			</div>
		</div>
	</div>

	<?php if (is_front_page()) { ?>
		<header class="head-search show-for-large">
			<div class="row">
				<div class="large-4 columns">
					<a href="/email-subscribe/" class="button">sign up for emails</a>
					<a href="/events/" class="button">events</a>
				</div>
<!-- 				<div class="large-2 columns">
					<a href="" class="button">upcoming events</a>
				</div> -->
				<div class="large-6 columns text-right">
					<p>get in touch with us! <span>205.623.2171 </span></p>
				</div>
				<div class="large-2 columns">
					<ul class="head-social">
						<li><a href="mailto:westfallpsychedu@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
						<li><a href="https://www.facebook.com/housecallslectureseries/?fref=ts" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/letsthinkonit" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</header>
		<section class="home-hero">
			<div class="hero-content-container">
				<div class="row hero-content">
					<div class="large-4 columns hero-button-col">
						<div class="blue-divide"></div>
						<div class="row">
							<div class="large-6 columns">
								<a href="#hero-anchor" class="blue-button button">lecture series</a>
							</div>
							<div class="large-6 columns">
								<a href="#event-anchor" class="blue-button button">event speakers</a>
							</div>
						</div>
						<div class="blue-divide"></div>
					</div>
					<div class="large-4 columns hero-logo-col">
						<a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/ltoi-logo.png" alt="logo" /></a>
						<div class="row arrow">
							<a href="#hero-anchor"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="large-4 columns hero-button-col">
						<div class="blue-divide"></div>
						<div class="row">
							<div class="large-6 columns">
								<a href="#podcasts-anchor" class="blue-button button">podcasts</a>
							</div>
							<div class="large-6 columns">
								<a href="#radio-anchor" class="blue-button button">radio</a>
							</div>
						</div>
						<div class="blue-divide"></div>
					</div>
				</div>
			</div>
		</section>

		<nav>
			<?php wp_nav_menu( array( 'theme_location' => 'main-nav' ) ); ?>
		</nav>
	<?php } else { ?>
		<header class="head-search show-for-medium sub-nav">
			<div class="row">
				<div class="medium-1 columns text-center">
					<a href="/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/sub-nav-logo.png" alt="logo" /></a>
				</div>
				<div class="medium-3 columns">
					<a href="/email-subscribe/" class="button">sign up for emails</a>
				</div>
				<div class="medium-6 columns nav">
					<?php wp_nav_menu( array( 'theme_location' => 'main-nav' ) ); ?>
				</div>
				<div class="medium-2 columns">
					<ul class="head-social">
						<li><a href="mailto:info@letsthinkonitnow.com"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
						<li><a href="https://www.facebook.com/housecallslectureseries/?fref=ts" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
						<li><a href="https://twitter.com/letsthinkonit" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</header>

	<?php } ?>


	<?php do_action( 'foundationpress_layout_start' ); ?>

	<section class="container">
		<?php do_action( 'foundationpress_after_header' );
