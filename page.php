<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header(); ?>

 <div id="sub-page" class="subpage" role="main">

    <?php do_action( 'foundationpress_before_content' ); ?>
  
    <?php if ( is_page('about-us') ) {
        $post   = get_post( 20 );
        $title = $post->post_title;
        $content = $post->post_content;
    ?>
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <div class="row">
                <div class="small-12 columns">
                    <header>
                      <h1 class="entry-title"><?php echo $title; ?></h1>
                    </header>
                </div>
            </div>
            <div class="row">
                <div class="medium-8 columns">
                    <div class="sub-content">
                        <?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?>
                        <?php echo $content; ?>
                    </div>
                </div>
                <div class="medium-4 columns about-hosts">
                    <h3>Meet the Podcast Hosts</h3>
                    <div class="cyan-divide"></div>
                    <?php $hosts = array ( 'post_type' => 'podcast-host', 'posts_per_page' => -1 );
                    query_posts($hosts);

                    while (have_posts()) : the_post(); ?>
                        <a data-open="post-<?php the_ID(); ?>">
                            <?php echo types_render_field( "podcast-host-image", array( "alt" => "host image", 'separator'=>'</div><div>') ) ?>
                            <h6><?php echo types_render_field( "podcast-host-name", array( 'separator'=>'</div><div>') ) ?></h6>
                        </a>

                        <div class="reveal" data-reveal id="post-<?php the_ID(); ?>">
                          <h3><?php echo types_render_field( "podcast-host-name", array( 'separator'=>'</div><div>') ) ?></h3>
                          <p><?php echo types_render_field( "podcast-host-image", array( "alt" => "host image", 'separator'=>'</div><div>') ) ?><?php echo types_render_field( "podcast-host-bio", array( 'separator'=>'</div><div>') ) ?></p>
                          <button class="close-button" data-close aria-label="Close modal" type="button">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </article>
    <?php } elseif (is_page('events')) { ?>
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <?php while ( have_posts() ) : the_post(); ?>
                <div class="row">
                    <div class="medium-10 medium-centered columns">
                        <header>
                          <h1 class="entry-title"><?php the_title(); ?></h1>
                        </header>

                        <div class="sub-content">
                          <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            <?php endwhile;?>
        </article>
    <?php } else { ?>
        <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
            <?php while ( have_posts() ) : the_post(); ?>
                <div class="row">
                    <div class="medium-8 medium-centered columns">
                        <header>
                          <h1 class="entry-title"><?php the_title(); ?></h1>
                        </header>

                        <div class="sub-content">
                          <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            <?php endwhile;?>
        </article>
    <?php } ?>
 </div>

 <?php get_footer();